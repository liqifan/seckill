package org.lqfan.service;

import org.lqfan.error.BusinessException;
import org.lqfan.service.model.UserModel;

public interface UserService {

    //通过用户id获取User对象
    UserModel getUserById(Integer id);

    void register(UserModel userModel) throws BusinessException;

    UserModel validateLogin(String telphone, String encrptPassword) throws BusinessException;
}
