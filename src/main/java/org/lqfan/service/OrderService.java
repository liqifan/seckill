package org.lqfan.service;

import org.lqfan.error.BusinessException;
import org.lqfan.service.model.OrderModel;

public interface OrderService {

    //创建订单
    OrderModel createOrder(Integer itemId, Integer userId, Integer amount) throws BusinessException;
}
