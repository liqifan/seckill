package org.lqfan.service;

import org.lqfan.error.BusinessException;
import org.lqfan.service.model.ItemModel;

import java.util.List;

public interface ItemService {

    //创建商品
    ItemModel createItem(ItemModel itemModel) throws BusinessException;

    //获取商品详情
    ItemModel getItemById(Integer id);

    //商品列表
    List<ItemModel> getListItem();

    //库存扣减
    boolean decreaseStock(Integer itemId, Integer amount);
}
