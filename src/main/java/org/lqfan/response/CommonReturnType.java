package org.lqfan.response;

public class CommonReturnType {
    //表明对应请求的返回处理结果“success”或“fail”
    private String status;

    //若status=success,则data内返回前端需要的数据
    //若status=fail,则data内使用通用的错误码格式
    private Object data;

    /**
     * 定义一个通用的创建方法
     * @param result
     * @return
     */
    public static CommonReturnType create(Object result) {
        return CommonReturnType.create(result, "success");
    }

    //成功时的返回
    public static CommonReturnType create(Object result, String status) {
        CommonReturnType returnType = new CommonReturnType();
        returnType.setData(result);
        returnType.setStatus(status);
        return returnType;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
